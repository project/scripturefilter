<?php

namespace Drupal\scripturefilter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

include_once 'scripturefilter.inc';

/**
 * Extension of FilterBase to perform this module's substitutions.
 *
 * @Filter(
 *   id = "scripturefilter",
 *   title = @Translation("Scripture Filter"),
 *   description = @Translation("Turns any Scripture reference into a link to one of several online Bibles."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class ScriptureFilter extends FilterBase {

  /**
   * A settings form for a site admin to configure default translation.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_trans = $this->settings['scripturefilter_default_translation'] ?? "NIV";
    $form['scripturefilter_default_translation'] = [
      '#type' => 'select',
      '#title' => $this->t('Bible Translation'),
      '#default_value' => $default_trans,
      '#description' => $this->t('Select which Bible version to use'),
      '#options' => [
        "KJ21" => $this->t("21st Century King James Version"),
        "ASV" => $this->t("American Standard Version"),
        "AMP" => $this->t("Amplified Bible"),
        "CEV" => $this->t("Contemporary English Version"),
        "DARBY" => $this->t("Darby Translation"),
        "ESV" => $this->t("English Standard Version"),
        "KJV" => $this->t("King James Version"),
        "MSG" => $this->t("The Message"),
        "NASB" => $this->t("New American Standard Bible"),
        "NET" => $this->t("New English Translation"),
        "NIRV" => $this->t("New International Reader's Version"),
        "NIV" => $this->t("New International Version"),
        "NIV1984" => $this->t("New International Version 1984"),
        "NIV-UK" => $this->t("New International Version - UK"),
        "NKJV" => $this->t("New King James Version"),
        "NLT" => $this->t("New Living Translation"),
        "NRSV" => $this->t("New Revised Standard Version"),
        "TNIV" => $this->t("Today's New International Version"),
        "WE" => $this->t("Worldwide English New Testament"),
        "WYC" => $this->t("Wycliffe New Testament"),
        "YLT" => $this->t("Young's Literal Translation"),
      ],
    ];
    return $form;
  }

  /**
   * Extension of base class process method, to replace Bible refs with links.
   */
  public function process($text, $langcode) {
    $translation = $this->settings['scripturefilter_default_translation'];
    $processed_text = scripturefilter_scripturize($text, $translation);
    return new FilterProcessResult($processed_text);
  }

  /**
   * Callback function for input filter tips() method.
   */
  public function tips($long = FALSE) {
    return $this->t('Scripture references will be linked automatically to an online Bible.  E.g. John 3:16, Eph 2:8-9 (ESV).');
  }

}
