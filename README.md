## INTRODUCTION

 - This is a simple filter module. It converts a Scripture reference into
   a clickable link that points to one of several on-line Bibles. The
   default is the NIV in English on biblegateway.com's site. It can also
   use the ESV online Bible and the NET Bible.

 - It is an adaptation of a PHP port of the Scripturizer Perl plug-in. A
   full list of credits is in the CREDITS file.

## DISCLAIMER

 - This module comes without any warranty of any kind, including any
   warranty (implied or otherwise) as to its fitness for a particular purpose.

## REQUIREMENTS

 - This module has no additional requirements.

## INSTALLATION

 - Simply download and enable the module.

## CONFIGURATION

 - Choose a text format that you want to be able to convert Bible references
   into links. In the list of Enabled Filters for that format, check
   "Scripture Filter". You can configure the default Bible translation to
   link when the content itself doesn't specify one. If necessary,
   rearrange the Filter Processing Order to avoid problems.
